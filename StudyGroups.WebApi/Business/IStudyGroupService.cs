﻿using StudyGroups.WebApi.Database.Models;
using System.Collections.Generic;

namespace StudyGroups.WebApi.Business
{
    public interface IStudyGroupService
    {
        void Add(List<Card> cards);
        IEnumerable<Card> GetGroup(string groupName);
        IEnumerable<string> GetGroupNames();
        void DeleteGroup(string groupName);
    }
}
