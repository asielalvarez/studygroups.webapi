﻿using MongoDB.Driver;
using StudyGroups.WebApi.Database;
using StudyGroups.WebApi.Database.Models;
using System.Collections.Generic;
using System.Linq;

namespace StudyGroups.WebApi.Business
{
    public class StudyGroupService : IStudyGroupService
    {
        private readonly IMongoCollection<Card> _cardsCollection;

        public StudyGroupService(IDbClient dbClient)
        {
            _cardsCollection = dbClient.GetCardsCollection();
        }

        public void Add(List<Card> cards)
        {
            _cardsCollection.InsertMany(cards);
        }

        public void DeleteGroup(string groupName)
        {
            _cardsCollection.DeleteMany(cards => cards.GroupName.Equals(groupName));
        }

        public IEnumerable<Card> GetGroup(string groupName) =>
            _cardsCollection.Find(cards => cards.GroupName.Equals(groupName))
            .ToEnumerable();

        public IEnumerable<string> GetGroupNames() =>
            _cardsCollection.AsQueryable().Select(x => x.GroupName).Distinct();
    }
}
